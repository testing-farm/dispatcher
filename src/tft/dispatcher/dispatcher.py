import random
import time
from typing import Any

import nomad
from gluetool.log import Logging, log_dict
from gluetool.utils import requests
from nomad.api.exceptions import BaseNomadException

from .config import settings

logger = Logging().get_logger()

LOOP_TICK = 1


def dispatch_request(host: str, job: str, request: dict[str, Any]) -> Any:
    tf_nomad = nomad.Nomad(host=host)

    try:
        logger.info(f"dispatching job {job} for request {request['id']}")
        return tf_nomad.job.dispatch_job(job, meta={"REQUEST_ID": request["id"]})
    except BaseNomadException as e:
        set_request_as_error(request, f"Failed to run job: {e}")
        return None


def get_request(api_url: str) -> Any:
    with requests() as req:
        response = req.get(f"{api_url}/v0.1/requests?state=new&ranch={settings.RANCH}")

    if not response:
        return None

    reqs = response.json()

    if reqs is None:
        return None

    log_dict(logger.info, "requests in 'new' state'", reqs)

    return random.choice(reqs)


def get_token(api_url: str, api_key: str, token_id: str) -> Any:
    with requests() as req:
        response = req.get(f"{api_url}/v0.1/tokens/{token_id}", headers={"Authorization": f"Bearer {api_key}"})

    if not response:
        return None

    token = response.json()

    if token is None:
        return None

    log_dict(logger.info, f"token '{token_id}'", token)

    return token


def set_request_as_error(request: dict[str, Any], message: str) -> bool:
    api_url: str = settings.INTERNAL_URL
    api_key: str = settings["API_KEY"]

    with requests() as req:
        response = req.put(
            f'{api_url}/v0.1/requests/{request["id"]}',
            json={
                "api_key": api_key,
                "state": "error",
                "notes": [{"level": "error", "message": message}],
            },
        )

    if not response:
        logger.error(f"Failed to set request as 'error': {response.text}")
        return False

    log_dict(logger.info, "Request updated", response.json())

    return True


def set_request_as_queued(api_url: str, request: dict[str, Any], details: Any = None) -> bool:
    api_key: str = settings["API_KEY"]

    payload: dict[str, Any] = {"state": "queued", "api_key": api_key}

    if details:
        payload.update(
            {
                "notes": [
                    {
                        "level": "info",
                        "message": details["DispatchedJobID"],
                    },
                ]
            }
        )

    with requests() as req:
        response = req.put(f'{api_url}/v0.1/requests/{request["id"]}', json=payload)

    if not response:
        logger.error(f"Failed to set request as 'queued': {response.text}")
        return False

    log_dict(logger.info, "Request updated", response.json())

    return True


def get_nomad_job(request: dict[str, Any]) -> Any:
    try:
        if request["settings"]["pipeline"]["type"] == "tmt-multihost":
            return settings["jobs"]["tmt-multihost"]
    except (KeyError, TypeError):
        pass

    if "sti" in request["test"] and request["test"]["sti"]:
        return settings["jobs"]["sti"]

    # TODO: current we support only 1 environment
    try:
        environment = request["environments_requested"][0]
    except (IndexError, KeyError, TypeError):
        environment = None

    # this is silly for now, we consider the request a functionl test if some compose passed
    if "fmf" in request["test"] and request["test"]["fmf"]:
        if (
            environment
            and "os" in environment
            and environment["os"]
            and "compose" in environment["os"]
            and environment["os"]["compose"]
        ):
            return settings["jobs"]["tmt"]

        return settings["jobs"]["tmt-container"]

    set_request_as_error(request, "This test is not yet supported by Testing Farm.")
    return None


def entrypoint() -> None:
    log_dict(logger.info, "settings", settings)

    for required in ["PUBLIC_URL", "INTERNAL_URL", "API_KEY"]:
        if required not in settings:
            logger.error(f"Required api setting {required} not found")

    public_api_url: str = settings.PUBLIC_URL
    internal_api_url: str = settings.INTERNAL_URL
    nomad_host: str = settings.HOST

    while True:
        logger.info("tick ...")
        time.sleep(LOOP_TICK)

        try:
            request = get_request(public_api_url)
        except Exception as error:
            log_dict(
                logger.error,
                "Getting request in 'new' state ended with error, retrying",
                error,
            )
            continue

        # no request found
        if not request:
            continue

        # fetch token that created the request
        token = get_token(public_api_url, settings.API_KEY, request["token_id"])

        if not token or not token["enabled"]:
            continue

        # find suitable job
        nomad_job = get_nomad_job(request)
        if not nomad_job:
            log_dict(
                logger.error,
                "No nomad job found, request went to error state.",
                request,
            )
            continue

        r_dispatch = dispatch_request(nomad_host, nomad_job, request)

        if not r_dispatch:
            set_request_as_error(request, "Testing Farm failed to dispatch this job.")
            log_dict(
                logger.error,
                "Failed running job on nomad, request went to error state.",
                request,
            )
            continue

        if not set_request_as_queued(internal_api_url, request, details=r_dispatch):
            set_request_as_error(request, "Testing Farm to queue this job.")
            log_dict(
                logger.error,
                "Failed to set job as queeud, request went to error state.",
                request,
            )
            continue
