#!/bin/sh

SETTINGS_YAML="$ROOT_PATH_FOR_DYNACONF/settings.yaml"

#
# Helpers functions
#

terminate() {
    # optional terminate command, can be used to wait for specific conditions when the container should finish
    if [ -n "TERMINATE_COMMAND" ]; then
        $TERMINATE_COMMAND
    fi

    # no pid means we failed too early
    if [ -z "$PID" ]; then
        echo "Skipping PID termination, entrypoint failed too early"
        exit 1
    fi

    echo "Terminating application"
    kill -TERM $PID

    # wait for the application to terminate
    wait $PID
}

# trap is needed to correctly propagate SIGTERM from container
trap terminate TERM INT

# helpers
error() { echo "Error: $@"; exit 1; }

#
# Wait for database to start
#

# Default value are used for testing and local development
TF_DISPATCHER_PUBLIC_URL=${TF_DISPATCHER_PUBLIC_URL:-$(yq eval '.default.PUBLIC_URL' "$SETTINGS_YAML")}
TF_DISPATCHER_INTERNAL_URL=${TF_DISPATCHER_INTERNAL_URL:-$(yq eval '.default.INTERNAL_URL' "$SETTINGS_YAML")}
TF_DISPATCHER_API_KEY=${TF_DISPATCHER_API_KEY:-$(yq eval '.default.API_KEY' "$SETTINGS_YAML")}
WAIT_TIMEOUT=${WAIT_TIMEOUT:-60}
WAIT_TICK=${WAIT_TICK:-1}

check_api() {
    echo "Checking api connection"
    wget -o/dev/null --quiet "$TF_DISPATCHER_PUBLIC_URL/v0.1/about"
    return $?
}

time=0
until check_api; do
    time=$((time + WAIT_TICK))
    [ $time -ge $WAIT_TIMEOUT ] && error "Failed to wait for database to start"
    sleep $WAIT_TICK
done

#
# Main script
#

COMMAND="tft-dispatcher"

# We run the command in background to get his PID which is used to properly
# terminate it with SIGTERM signal.

$COMMAND &
PID=$!

wait $PID
