.DEFAULT_GOAL := help

.PHONY: init install dev dev/stop dispatcher/start dispatcher/stop clean

# Configurables
API_PUBLIC_PORT ?= 8001
TESTING_FARM_API_TOKEN ?= developer

# Variables
ROOT_DIR = $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
DEVELOPMENT_DATA = $(ROOT_DIR)/.dev

DISPATCHER_PID_FILE = $(DEVELOPMENT_DATA)/dispatcher-pid-file

API_ABOUT = http://localhost:$(API_PUBLIC_PORT)/v0.1/about

IMAGE_NAME ?= quay.io/testing-farm/dispatcher
# default image tag set to current user name
IMAGE_TAG ?= ${USER}

# Help prelude
define PRELUDE

Usage:
  make [target]

endef

# Help targets
init:
	@mkdir -p $(DEVELOPMENT_DATA)

##@ Install
install:  ## Run poetry install
	poetry env use python3.9
	poetry install

##@ Development Environment
dev: dispatcher/start  ## Start development environment
dev/stop: dispatcher/stop  ## Stop development environment

##@ Dispatcher
dispatcher/start: init  ## Start dispatcher
	@if test -e $(DISPATCHER_PID_FILE); then \
		echo "[+] Dispatcher is started"; \
		exit; \
	fi;
	@if ! poetry run command -v tft-dispatcher >/dev/null; then \
		echo "[E] Dispatcher is not installed, run 'make install' first."; \
		exit 1; \
	fi;
	@if ! curl -s -o /dev/null $(API_ABOUT); then \
			echo "[E] Testing Farm API is not running, cannot continue"; \
			exit 1; \
	fi;
	echo "[+] Starting dispatcher"; \
	echo -e "\033[33m[!] Use 'make dev/stop' to stop dispatcher\033[0m"; \
	poetry run tft-dispatcher & \
	echo $$! > $(DISPATCHER_PID_FILE);

dispatcher/stop:  ## Stop dispatcher
	@if test -e $(DISPATCHER_PID_FILE); then \
		echo "[+] Stopping dispatcher"; \
		kill $$(cat $(DISPATCHER_PID_FILE)); \
		rm -f $(DISPATCHER_PID_FILE); \
	else \
		echo "[+] Dispatcher is stopped"; \
	fi;

##@ Containers

container/build:  ## Build containers
	poetry build
	poetry export -f requirements.txt --output requirements.txt
	buildah bud --cache-from $(IMAGE_NAME) -t $(IMAGE_NAME):$(IMAGE_TAG) -f container/Containerfile .
	rm -rf requirements.txt dist/

container/push:  ## Push containers
	buildah push $(IMAGE_NAME):$(IMAGE_TAG)

container/test:  ## Test container image via dgoss
	cd container && dgoss run --stop-timeout 0 -t --entrypoint sleep $(IMAGE_NAME):$(IMAGE_TAG) infinity

##@ Utility
clean: dev/stop  ## Cleanup
	rm -rf $$(poetry run bash -c 'echo $$VIRTUAL_ENV')
	rm -rf $(DEVELOPMENT_DATA)

# See https://www.thapaliya.com/en/writings/well-documented-makefiles/ for details.
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

help:  ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "$(info $(PRELUDE))"} /^[a-zA-Z_/-]+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(call reverse, $(MAKEFILE_LIST))
